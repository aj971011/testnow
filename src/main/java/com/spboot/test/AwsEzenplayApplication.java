package com.spboot.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class AwsEzenplayApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsEzenplayApplication.class, args);
	}

	
}